require 'rails_helper'
RSpec.describe GamesController, type: :controller do
  let!(:game) { create('game') } 
  describe 'POST #create' do
    it 'create new and do js code' do

      params_game = attributes_for('game')

      expect {
        post :create, params: { game: params_game }, format: :js
      }.to change(Game, :count).by(1)
      expect(response).to be_successful
    end
  end

  describe 'PUT #update' do
    it 'update success and render message updated' do
      params_game = attributes_for('game')
      put :update, params: { id: game.id, "winner"=>"2", "player_1_moves"=>{"0"=>["2", "1"], "1"=>["1", "2"], "2"=>["1", "0"]}, "player_2_moves"=>{"0"=>["2", "1"], "1"=>["1", "2"], "2"=>["1", "0"]} }
      expect(response).to be_successful
      expect(response_body['message']).to eq('Updated.')
      expect(assigns(:game).is_done).to eq(true)
    end
  end
  
  private
  def response_body
    JSON.parse(response.body)
  end
end
