require 'rails_helper'

RSpec.describe HomeController, type: :controller do
   describe "GET index" do
    it "assigns @game" do
      board_size = 3
      max_moves= 9
      get :index
      expect(assigns(:game).present?).to eq(true)
      expect(assigns(:board_size)).to eq(board_size)
      expect(assigns(:max_moves)).to eq(max_moves)
    end

    it "renders the index template" do
      get :index
      expect(response).to render_template("index")
    end
  end
end
