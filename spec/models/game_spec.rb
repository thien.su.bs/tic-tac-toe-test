require 'rails_helper'

RSpec.describe Game, type: :model do
  describe 'validation' do
    Game.validators.each do |e|
      if e.class.to_s == 'ActiveRecord::Validations::PresenceValidator'
        e.attributes.each do |attribute|
          it { should validate_presence_of(attribute) }
        end
      end
    end
  end
end
