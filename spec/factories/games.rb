require 'faker'
FactoryBot.define do
  factory 'game', class: 'Game' do
    player_1 { Faker::Lorem.characters(number: 10) }
    player_2 { Faker::Lorem.characters(number: 10) } 
  end
end