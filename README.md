# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version: 2.6.3

* System dependencies: ```bundle -j 4```

* Configuration

* Database creation (rails db:create, rails db:migrate)

* Database initialization

* How to run the test suite 

    
  1. ```bundle exec rspec spec/ ```

  2. ```bundle exec rspec spec/models```

  3. ```bundle exec rspec spec/controllers```
   

* Run Rails application: 
  ```rails s```


* ...
