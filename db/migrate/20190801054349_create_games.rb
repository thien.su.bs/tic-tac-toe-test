class CreateGames < ActiveRecord::Migration[5.2]
  def change
    create_table :games do |t|
      t.string :player_1
      t.string :player_2
      t.boolean :is_done, default: false
      t.integer :winner
      t.timestamps
    end
  end
end
