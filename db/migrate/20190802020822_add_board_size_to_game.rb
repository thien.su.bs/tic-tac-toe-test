class AddBoardSizeToGame < ActiveRecord::Migration[5.2]
  def change
    add_column :games, :board_size, :integer, default: 3
  end
end
