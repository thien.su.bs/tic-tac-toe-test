class AddPlayerMovesToGame < ActiveRecord::Migration[5.2]
  def change
    add_column :games, :player_1_moves, :string
    add_column :games, :player_2_moves, :string
  end
end
