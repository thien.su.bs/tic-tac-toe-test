class GamesController < ApplicationController

  # Create Game Information
  def create
    @game = Game.new(params_game)
    if @game.save
      @is_save = true
      @game_id = @game.id
      @game = Game.new
    else
      @is_save = false
    end
    @board_size = params_game[:board_size].to_i
    @board_size = 3 if @board_size < 3 
    @max_moves= @board_size**2
  end

  #update when winning or no moves.
  def update
    @game = Game.find(params[:id])
    return render json: {message: "Not found."} unless @game.present? 
    return render json: {message: "Can not update."} if @game.is_done
    if @game.update(is_done: true, winner: params_game_winner[:winner].to_i, 
      player_1_moves: params_game_winner[:player_1_moves], player_2_moves: params_game_winner[:player_2_moves])
      render json: {message: "Updated."}
    else
      render json: {message: "Fail: {#{@game.errors.full_messages.join('\n')}}"}
    end
  end

  private
  def params_game_winner
    params[:player_1_moves] = JSON.dump(params[:player_1_moves])
    params[:player_2_moves] = JSON.dump(params[:player_2_moves])
    params.permit(:winner, :player_1_moves, :player_2_moves)    
  end
  def params_game
    params.require(:game).permit(:player_1, :player_2, :board_size)
  end
end
