class HomeController < ApplicationController
  def index
    @board_size = 3
    @max_moves = @board_size**2
    @game = Game.new
  end
end
