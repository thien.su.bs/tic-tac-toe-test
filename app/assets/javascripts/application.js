// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require jquery/dist/jquery
//= require turbolinks
//= require_tree .

$(document).on('turbolinks:load', function () {
  $.ajaxSetup({
      headers:
      { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
  });

  init_data();
});
var game;
function init_data(){
  game = {
    n: 6,
    first: true,
    player_1: [],
    player_1_detail: {x: [], y: []},
    player_2: {x: [], y: []},
    player_2: [],
    player_2_detail: {x: [], y: []},
    mark_1: 'x',
    mark_2: 'o',
    currentPlayer: true, //player_1: true, player_2: false
    moves: 0,
    end_game: 36,
    winner: 0, // [0, 1, 2], 0: Draw.
  };
}


function icon(id) {
  var row = $('#' + id).data('row');
  var col = $('#' + id).data('column');

  if (game.currentPlayer){
    $('#' + id).html(game.mark_1);
    $('#' + id).removeAttr('onClick');
    $('#' + id).removeClass('init');
    $('#' + id).addClass('x')
    game.player_2.push([col, row])
    game.player_2_detail.x.push(col);
    game.player_2_detail.y.push(row);
    game.currentPlayer = false;
    game.moves++;
  }
  else{
    $('#' + id).html(game.mark_2);
    $('#' + id).removeAttr('onClick');
    $('#' + id).removeClass('init');
    $('#' + id).addClass('o');
    game.player_1.push([col, row])
    game.player_1_detail.x.push(col);
    game.player_1_detail.y.push(row);
    game.currentPlayer = true;
    game.moves++;
  }

  if (is_win()){
    $('#myModal').show();
    $('.game-field').removeAttr('onClick');
    update_remote();
    init_data()
  }

  if (game.moves>=Math.pow(game.n, 2))
  {
    // Draw, No More Moves
    $('#myModal').show();
    update_remote()
    init_data();
  }

}
function isItemInArray(array, item) {
    for (var i = 0; i < array.length; i++) {
        // This if statement depends on the format of your array
        if (array[i][0] == item[0] && array[i][1] == item[1]) {
            return true;   // Found it
        }
    }
    return false;   // Not found
}
function desc(a, b){return a - b}

function is_win(){
  if ((game.player_1.length <= game.end_game && game.player_1.length >game.n-1)  ||
    (game.player_2.length <= game.end_game && game.player_2.length > game.n-1))
  {
    var is_player_1_win = case_cross(game.player_1);
    var is_player_2_win = case_cross(game.player_2);
    if (is_player_1_win)
      game.winner=1;
    if (is_player_2_win)
      game.winner=2;
    var row_player_1 = case_row(game.player_1, game.player_1_detail);
    var row_player_2 = case_row(game.player_2, game.player_2_detail);
    if (row_player_1)
      game.winner=1;
    if (row_player_2)
      game.winner=2;
    var col_player_1 = case_column(game.player_1, game.player_1_detail);
    var col_player_2 = case_column(game.player_2, game.player_2_detail);
    if (col_player_1)
      game.winner=1;
    if (col_player_2)
      game.winner=2;
    return (is_player_1_win || is_player_2_win || row_player_1 || row_player_2 || col_player_1 || col_player_2);

  }
}
function update_remote(){
  var game_id = $( "#game_id" ).val();
  var request = $.ajax({
    url: "games/"+game_id,
    method: "PUT",
    data: {  winner: game.winner, player_1_moves: game.player_1, player_2_moves:  game.player_1},
    dataType: "json"
  });
   
  request.done(function( msg ) {
    console.log(msg);
  });
   
  request.fail(function( jqXHR, textStatus ) {
    console.log(textStatus)
  });
}

function case_cross(input_array){
  var i;
  var flag_pri = true;
  var flag_second = true;
  for (i = 0; i < game.n; i++) { 
    if (!isItemInArray(input_array , [i,i]))
      flag_pri = false;
    if (!isItemInArray(input_array , [i,game.n-1-i]))
      flag_second = false;
  }
  if (flag_pri || flag_second){
    return true;
  }
  else{
    return false;
  }
}
function case_row(input_array, array_y){
  var flag=true;
  array_temp = array_y.y;
  var map = array_y.y.reduce(function(prev, cur) {
    prev[cur] = (prev[cur] || 0) + 1;
    return prev;
  }, {});
  var key;
  for (var property1 in map) {
    if (map[property1] >= game.n)
    {
      for (var i = 0; i < game.n; i++) {
        if (!isItemInArray(input_array ,[i, property1]))
          flag = false;
      }
      if (flag) return flag;
    }
  }
}
function case_column(input_array, array_x){
  var flag = true;
  array_temp = array_x.x;
  var map = array_x.x.reduce(function(prev, cur) {
    prev[cur] = (prev[cur] || 0) + 1;
    return prev;
  }, {});
  var key;
  for (var property1 in map) {
    if (map[property1] >= game.n)
    {
      for (var i = 0; i < game.n; i++) {
        if (!isItemInArray(input_array ,[property1, i]))
          flag = false;
      }
      if (flag) return flag;
    }
  }
}