class Game < ApplicationRecord
  ##Validations
  validates :player_1, presence: true
  validates :player_2, presence: true
end
